= check-passphrase-gpg

A script to check for the correct passphrase from a given passphrase file for a
given OpenPGP key of a given user ID stored in the GnuPG keyring.

== Usage

=== Nix Flakes

[source,console]
----
> nix run gitlab:zoonfafer/check-passphrase-gpg USER_ID PASSPHRASE_FILE
----
