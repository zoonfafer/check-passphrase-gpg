{
  description = "Check Passphrase for GPG key";
  inputs = rec {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils,... }@inputs:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          packageName = "check-passphrase-gpg";
          app = pkgs.callPackage ./default.nix {};
        in
        rec {
          defaultPackage = self.packages.${system}.${packageName};
          packages.${packageName} = app;
        }
      );
}
