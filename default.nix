# See: https://nixos.wiki/wiki/Shell_Scripts#Packaging
#
{ pkgs
, lib
, stdenvNoCC
, bash
, gnupg
, makeWrapper
}:
stdenvNoCC.mkDerivation {
  pname = "check-passphrase-gpg";
  version = "0.0.1";
  src = ./.;
  buildInputs = [ bash gnupg ];
  nativeBuildInputs = [ makeWrapper ];
  installPhase = ''
      mkdir -p $out/bin
      cp bin/check-passphrase-gpg $out/bin/check-passphrase-gpg
      wrapProgram $out/bin/check-passphrase-gpg \
        --prefix PATH : ${lib.makeBinPath [ bash gnupg ]}
  '';
}
